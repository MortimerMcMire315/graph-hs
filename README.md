graph-hs
========
A purely functional library for dealing with graph theory problems in Haskell.

Installation
------------
1. Download the [Haskell Platform](https://www.haskell.org/) or install it using a package manager.
2. Install [cabal](https://www.haskell.org/cabal/), the Haskell package manager and build system.
3. run `cabal build` to build the GraphTheory library.

You may also try the following:
* run `cabal build graphtest` to build Main.hs.
* run `cabal run graphtest` to run Main.hs.
* run `cabal install` to install the GraphTheory library on your machine. You may have to add ~/.cabal/bin to your $PATH on a Unix-like machine.
